from datetime import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium .webdriver.common.by import By
import pandas as pd

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://hk.centanet.com/findproperty/list/transaction?q=xKIODZ7vwUGonNSLuYAWbw")

cells =  driver.find_elements(By.CSS_SELECTOR, ".right-block")
# len(cells)

click_button = driver.find_elements(By.CSS_SELECTOR, ".btn-next")
checking = driver.find_elements(By.CSS_SELECTOR, ".number.active")
record_list = []
#
# def click_next_page():
#     if len(checking) == 0:
#         click_button.click()

if len(checking) > 0:
    for cell in cells:
        price = cell.find_element(By.CSS_SELECTOR, ".price-block.is-sale > .price").text
        name = cell.find_element(By.CSS_SELECTOR, ".title-lg").text
        date = cell.find_element(By.CSS_SELECTOR, ".price-block.is-date > .date").text
        record_list.append([name, price, date])
        click_button.click()
        time.sleep(5)

df = pd.DataFrame(record_list, columns=['地址', '成交價', '成交日期'])



